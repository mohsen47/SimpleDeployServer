#!/bin/bash

pip install -r requirements.txt

# add a non-privileged user to complete the tests
adduser test --disabled-password --gecos ""
