#!/usr/bin/python
import BaseHTTPServer
import sys
import signal
import json
import thread
import ConfigParser
import os
import re
import shutil

from daemon import Daemon
import argparse

from git import Repo
from git.exc import InvalidGitRepositoryError, NoSuchPathError, GitCommandError

def signal_handler(signal, frame):
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

class MyDaemon(Daemon):
    def run(self):
        HOME_DIR = os.environ["HOME"]
        CONFIG_DIR = HOME_DIR +  "/.deploy/"
        CONFIG_FILE = CONFIG_DIR + "options.ini"
        config = ConfigParser.ConfigParser()
        try:
            config.readfp(open(CONFIG_FILE))
        except IOError as e:
            print("Config file %s not found" % CONFIG_FILE)
            sys.exit(1)
        except ConfigParser.ParsingError as e:
            print("Config file %s cannot be parsed" % CONFIG_FILE)
            sys.exit(1)
        if not config.has_section("server"):
            print("Config file %s does not include the server section" % CONFIG_FILE)
            sys.exit(1)
        try:
            host = config.get("server", "host")
        except ConfigParser.NoOptionError as e:
            print("Section server in config file %s does not include the host option" % CONFIG_FILE)
            sys.exit(1)
        try:
            port = int(config.get("server", "port"))
        except ConfigParser.NoOptionError as e:
            print("Section server in config file %s does not include the port option" % CONFIG_FILE)
            sys.exit(1)
        except ValueError as e:
            print("Cannot convert port in section server of config file %s to integer" % CONFIG_FILE)
            sys.exit(1)
        if port <= 1024:
            print("Port must be greater than 1024")
            sys.exit(1)

        server_address = (host,port)

        httpd = BaseHTTPServer.HTTPServer(server_address, MyRequestHandler)
        try:
            httpd.serve_forever()
        except Exception as e:
            sys.exit(0)

    def status(self):
        try:
            pf = file(self.pidfile,'r')
        except IOError as e:
            return False
        pid = int(pf.read().strip())
        pf.close()
        try:
            os.kill(pid, 0)
        except OSError as e:
            return False
        else:
            return True


class MissingSectionError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class MissingOptionError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class MyRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def checkConfig(self, CONFIG_FILE, url):
        # Check the configuration file
        try:
            c = Config(CONFIG_FILE)
        except IOError as e:
            print("File %s not found" % CONFIG_FILE)
            return False
        except ConfigParser.ParsingError as e:
            print("Unable to parse file %s" % CONFIG_FILE)
            return False
        except MissingSectionError as e:
            print("Section %s does not exist in file %s" % (str(e), CONFIG_FILE))
            return False
        except ConfigParser.NoOptionError as e:
            print("%s in %s" % (str(e), CONFIG_FILE))
            return False

        # Check for a valid bare git repository
        os.environ["GIT_DIR"] = c.getOption("repo", "git_dir")
        try:
            repo = Repo(c.getOption("repo", "git_dir"))
            assert repo.bare
            repo.remotes['origin']
        except NoSuchPathError as e:
            try:
                os.mkdir(c.getOption("repo", "git_dir"))
                repo = Repo.init(c.getOption("repo", "git_dir"), bare=True)
                repo.create_remote("origin", url)
                return True
            except OSError as e:
                print("directory %s cannot be found and cannot be created" % c.getOption("repo", "git_dir"))
                return False
        except InvalidGitRepositoryError as e:
            repo = Repo.init(c.getOption("repo", "git_dir"), bare=True)
            repo.create_remote("origin", url)
            return True
        except AssertionError as e:
            shutil.rmtree(c.getOption("repo", "git_dir") + ".git")
            repo = Repo.init(c.getOption("repo", "git_dir"), bare=True)
            repo.create_remote("origin", url)
            return True
        except IndexError as e:
            repo.create_remote("origin", url)
            return True
        except GitCommandError as e:
            print("Make sure the remote repo exists and you can pull from it")
            return False
        self.config = c
        return True
    def do_POST(self):
        # First things first, send respone to gitlab as fast as possible
        self.send_response(200)
        message = "OK"
        self.send_header("Content-type", "text")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)
        # Read the POST request body and make sure it is a Push Event
        size = int(self.headers["Content-length"])
        try:
            eventType = self.headers["X-Gitlab-Event"]
        except KeyError as e:
            print("Wrong request missing X-Gitlab-Event header")
            return
        if eventType == "Push Hook":
            HOME_DIR = os.environ["HOME"]
            CONFIG_DIR = HOME_DIR +  "/.deploy/"
            dataJson = json.loads(self.rfile.read(size))
            ref = dataJson["ref"].split("/")[2]
            remoteRepo = dataJson["project"]["path_with_namespace"].replace("/", ".")
            repoSSHUrl = dataJson["project"]["git_ssh_url"]
            if self.checkConfig(CONFIG_DIR + str(remoteRepo), repoSSHUrl):
                self.processPOST(ref)

    def resetWorkTree(self, localRepo, workTree, remoteBranch):
        repo = Repo(localRepo)

        if not os.path.isdir(workTree):
            try:
                os.mkdir(workTree)
            except OSError as e:
                if re.search("Permission denied", str(e)):
                    print("Unable to create the work tree for branch %s %s" % (remoteBranch, str(e)))
                else:
                    print("Unkonwn system error %s" % str(e))
                return
        workTree = os.path.abspath(workTree)
        os.environ["GIT_DIR"] = os.path.abspath(localRepo)
        os.environ["GIT_WORK_TREE"] = workTree

        repo.git.fetch("origin")
        repo.git.reset("--hard", "origin/" + remoteBranch)

    def updateWorkTree(self, ref, config):
        if config.hasBranch(ref):
            try:
                work_tree = config.getOption(ref, "work_tree")
                self.resetWorkTree(config.getOption("repo", "git_dir"), work_tree, ref)
            except ConfigParser.NoSectionError as e:
                try:
                    work_tree = config.getOption("repo", "work_tree")
                    self.resetWorkTree(config.getOption("repo", "git_dir"), work_tree, ref)
                except ConfigParser.NoOptionError as e:
                    print("This branch %s does not have a work_tree and no default work_tree present" % ref)
                    return
            except ConfigParser.NoOptionError as e:
                try:
                    work_tree = config.getOption("repo", "work_tree")
                    self.resetWorkTree(config.getOption("repo", "git_dir"), work_tree, ref)
                except ConfigParser.NoOptionError as e:
                    print("This branch %s does not have a work_tree and no default work_tree present" % ref)
                    return
        else:
            print("No branch named %s present" % ref)
            return

    def processPOST(self, ref):
        # Get the branch name and update the work tree for this branch
        self.updateWorkTree(ref, self.config)

class Config:
    def __init__(self, fileName):
        self.fileName = fileName
        self.config = ConfigParser.ConfigParser()
        self.config.readfp(open(fileName))

        if not self.config.has_section("repo"):
            raise MissingSectionError("repo")
        if self.config.get("repo", "git_dir") == "":
            raise ConfigParser.NoOptionError("git_dir","")

    def hasSection(self, name):
        return self.config.has_section(name)

    def hasOption(self, section, name):
        return self.config.has_option(section, name)

    def hasBranch(self, name):
        if self.hasSection(name):
            return True
        try:
            branches = self.getOption("repo", "branches").split(",")
            if name in branches:
                return True
        except ConfigParser.NoOptionError as e:
            return False
        return False


    def getOption(self, section, name):
        return self.config.get(section, name)

    def hasOption(self, section, name):
        return self.config.has_option(section, name)

    def hasBranch(self, name):
        if self.hasSection(name):
            return True
        try:
            branches = self.getOption("repo", "branches").split(",")
            if name in branches:
                return True
        except ConfigParser.NoOptionError as e:
            return False
        return False

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", choices=["start","stop","restart", "status"])
    args = parser.parse_args()

    if args.d:
        HOME_DIR = os.environ["HOME"]
        STDOUT = "%s/.deploy/deploy.out" % HOME_DIR
        STDERR = "%s/.deploy/deploy.err" % HOME_DIR
        PID = "%s/.deploy/deploy.pid" % HOME_DIR
        myDaemon = MyDaemon(PID, '/dev/null', STDOUT, STDERR)
        if args.d == "start":
            myDaemon.start()
        elif args.d == "stop":
            myDaemon.stop()
        elif args.d == "restart":
            myDaemon.restart()
        else:
            if myDaemon.status():
                print("daemon is running")
            else:
                print("daemon is NOT running")
    else:
        HOME_DIR = os.environ["HOME"]
        CONFIG_DIR = HOME_DIR +  "/.deploy/"
        CONFIG_FILE = CONFIG_DIR + "options.ini"
        config = ConfigParser.ConfigParser()
        try:
            config.readfp(open(CONFIG_FILE))
        except IOError as e:
            print("Config file %s not found" % CONFIG_FILE)
            sys.exit(1)
        except ConfigParser.ParsingError as e:
            print("Config file %s cannot be parsed" % CONFIG_FILE)
            sys.exit(1)
        if not config.has_section("server"):
            print("Config file %s does not include the server section" % CONFIG_FILE)
            sys.exit(1)
        try:
            host = config.get("server", "host")
        except ConfigParser.NoOptionError as e:
            print("Section server in config file %s does not include the host option" % CONFIG_FILE)
            sys.exit(1)
        try:
            port = int(config.get("server", "port"))
        except ConfigParser.NoOptionError as e:
            print("Section server in config file %s does not include the port option" % CONFIG_FILE)
            sys.exit(1)
        except ValueError as e:
            print("Cannot convert port in section server of config file %s to integer" % CONFIG_FILE)
            sys.exit(1)
        if port <= 1024:
            print("Port must be greater than 1024")
            sys.exit(1)

        server_address = (host,port)

        httpd = BaseHTTPServer.HTTPServer(server_address, MyRequestHandler)
        try:
            httpd.serve_forever()
        except Exception as e:
            sys.exit(0)